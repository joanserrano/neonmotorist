#ifndef PLAYER_H
#define PLAYER_H

#include "Entity.h"

class Player : public Entity
{
	public:
		Player();
		~Player();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);

		virtual void render(int offX = 0, int offY = 0);
		virtual void update();

		void updateControls();
		
		bool isOfClass(std::string classType);
		std::string getClassName(){return "Player";};

	protected:
};

#endif
