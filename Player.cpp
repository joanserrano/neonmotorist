//Include our classes
#include "Player.h"
#include "singletons.h"


Player::Player() : Entity(){
	mpSpeedX = 300;
	mpSpeedY = 300;
	mpSpeedMax = mpSpeedY;
	mpSpeedMin = 10;
	mpFriction = 0.92;

}

Player::~Player(){
}

void Player::init(){
	Entity::init();
}
void Player::init(int x, int y){
	Entity::init(x, y);
}
void Player::init(int graphic, int x, int y, int w, int h){
	Entity::init(graphic, x, y, w, h);

	mpGraphicRect = C_Rectangle{ 0,0,64,64 };
}

void Player::render(int offX, int offY) {
	Entity::render(0, 32);
}

void Player::update() {
	Entity::update();

}

void Player::updateControls() {
	mpDirectionX = NONE;
	mpDirectionY = NONE;
	if (key_down['W'] || key_down['w']) {
		
		mpDirectionY = UP;
	}
	if (key_down['A'] || key_down['a']) {
		
		mpDirectionX = LEFT;
	}
	if (key_down['S'] || key_down['s']) {
		
		mpDirectionY = DOWN;
	}
	if (key_down['D'] || key_down['d']) {
	
		mpDirectionX = RIGHT;
	}
	return;
}

bool Player::isOfClass(std::string classType){
	if(classType == "Player" || 
		classType == "Entity"){
		return true;
	}
	return false;
}

