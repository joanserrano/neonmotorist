#ifndef ENTITY_H
#define ENTITY_H

#include "ofMain.h"
#include "includes.h"
#include "Renderer.h"
#include "Utils.h"

class Entity
{
	public:
		Entity();
		~Entity();
		
		virtual void init();
		virtual void init(int x, int y);
		virtual void init(int graphic, int x, int y, int w, int h);
		virtual void render(int offX = 0, int offY = 0);
		virtual void update();

		void setX(int x);
		void setY(int y);
		void setXY(int x, int y);
		
		void setW(int w);
		void setH(int h);
		void setRectangle(C_Rectangle rect);
		void setRectangle(int x, int y, int w, int h);

		bool getAlive() { return mpAlive; };
		void setAlive(bool alive);

		bool isInsideRectangle(C_Rectangle a_rect);

		int getX()		{return mpRect.x;};
		int getY()		{return mpRect.y;};
		int getW()		{return mpRect.w;};
		int getH()		{return mpRect.h;};
		int getDepth()  {return mpDepth; }; //! Returns the mpDepth Param


		
		C_Rectangle getRect() { return mpRect; };

		virtual bool isOfClass(std::string classType);
		virtual std::string getClassName(){return "Entity";};

	protected:
		virtual void updateControls();
		bool checkCollisionWithMap();
		bool checkCollisionLine(int x1, int y1, int x2, int y2, int disPx);

		void move();

		virtual void updateGraphic();

		C_Rectangle 	mpRect;	//Collision and Position
		C_Rectangle 	mpGraphicRect;
		int				mpGraphicImg;
		int				mpDepth; //! the actual depth (z in 3D) of the sprite, to make the order in layer

		int				mFrame;
		int				mMaxFrame;
		int				mCurrentFrameTime;
		int				mMaxFrameTime;
	
		bool 			mpAlive;

		//! We need 2 types of direction because we want that the player can change directions while moving
		int				mpDirectionX;
		int				mpDirectionY; 

		//Velocity
		int				mpSpeedX;
		int				mpSpeedY;
		int				mpSpeedMax; //! Max speed for the entity
		int				mpSpeedMin; //! Min speed for the entity
		float			mpFriction; //! Friction used to slow down the motorbike in iddle
		int				mpSignDirection; //! A sign that is used for the player if its in foward direction or lower direction



		bool			mpMoving;

		int				mpXtoGo;
		int				mpYtoGo;

		int				mpInitialX;
		int				mpInitialY;
		
		bool			mpTileBasedMovement;
};

#endif
