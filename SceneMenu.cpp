#include "singletons.h"

//Include our classes
#include "SceneMenu.h"

SceneMenu::SceneMenu(): Scene(){ //Calls constructor in class Scene
}

SceneMenu::~SceneMenu(){
}

void SceneMenu::init(){
	Scene::init(); //Calls to the init method in class Scene
	mpGraphicID = sResManager->getGraphicID("test.png");
	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = sResManager->getGraphicWidth(mpGraphicID);
	mpGraphicRect.h = sResManager->getGraphicHeight(mpGraphicID);
}

void SceneMenu::load(){
	Scene::load(); //Calls to the init method in class Scene
}

void SceneMenu::updateScene(){
	inputEvent();
}

void SceneMenu::drawScene(){
	ofSetColor(255, 0, 0);
	ofDrawRectangle(50, 50, 100, 100);
	imgRender(mpGraphicID, 200, 300, mpGraphicRect, 255);
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneMenu::inputEvent(){
	//if (key_released[13]) {	// ENTER
	//if (key_released[8]) {	// BACKSPACE
	//if (key_released[9]) {	// TAB
	//if (key_released[127]) {	// SUPR
	if (key_released[' ']) { //Space
		sDirector->changeScene(SceneDirector::LEVEL);
	}
}
