//Include our classes
#include "Entity.h"
#include "singletons.h"


Entity::Entity() {
	mpAlive = true;
	setRectangle(0, 0, TILE_SIZE, TILE_SIZE);

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = 0;
	mpGraphicRect.h = 0;

	mpGraphicImg = -1;

	mFrame = 0;
	mMaxFrame = 0;
	mCurrentFrameTime = 0;
	mMaxFrameTime = 150;

	mpDirectionX = NONE;
	mpDirectionY = NONE;
	mpSpeedX = 0;
	mpSpeedY = 0;
	mpSpeedMax = mpSpeedX;
	mpSpeedMin = 0;
	mpSignDirection = 0;
	mpFriction = 1;


	mpMoving = false;

	mpXtoGo = mpRect.x;
	mpYtoGo = mpRect.y;

	mpTileBasedMovement = false;

}

Entity::~Entity() {
}

void Entity::init() {
	mpAlive = true;
}

void Entity::init(int x, int y) {
	init();
	mpInitialX = x;
	mpInitialY = y;

	mpRect.x = mpInitialX;
	mpRect.y = mpInitialY;
	mpDepth = -mpRect.y;

	mpXtoGo = mpRect.x;
	mpYtoGo = mpRect.y;
}

void Entity::init(int graphic, int x, int y, int w, int h) {
	init(x, y);
	mpRect.w = w;
	mpRect.h = h;
	mpGraphicImg = graphic;
	mpGraphicRect = C_Rectangle{ 0,0,(unsigned int)w,(unsigned int)h };
}

void Entity::update() {
	if (!mpAlive) { return; }
	updateControls();
	move();
	updateGraphic();
}

void Entity::updateGraphic() {
	mCurrentFrameTime += global_delta_time;
	if (mCurrentFrameTime > mMaxFrameTime) {
		mCurrentFrameTime = 0;
		mFrame++;
		if (mFrame >= mMaxFrame) {
			mFrame = 0;
		}
	}
	mpGraphicRect.x = mFrame*mpGraphicRect.w;
}

void Entity::render(int offX, int offY) {
	if (!mpAlive || mpGraphicImg < 0) { return; }
	imgRender(mpGraphicImg, mpRect.x - offX, mpRect.y - offY, mpGraphicRect);
}

void Entity::updateControls() {

	return;
}

void Entity::move() {
	int xx = mpRect.x;
	int yy = mpRect.y;
	
	//! Because we have acceleration we need to see if the player doesn't surpass the maxSpeed
	if (mpSpeedX > mpSpeedMax || mpSpeedX < mpSpeedMin) {

		if (mpSpeedX <= mpSpeedMin) {
			mpSpeedX = mpSpeedMin;
			mpSignDirection = 1;
		}
		else if (mpSpeedX > mpSpeedMax) {
			mpSpeedX = mpSpeedMax;
		}
	}

	//! we switch the speed and previous direction depending if  the player has gone left or right

	//! for the up/down value, that uses MRU;
	switch (mpDirectionY) {
	case UP:
		mpSpeedY = mpSpeedMax * 0.8;
		yy = yy - mpSpeedY*global_delta_time / 1000;
		break;
	case DOWN:
		mpSpeedY = mpSpeedMax * 0.8;
		yy = yy + mpSpeedY*global_delta_time / 1000;
		break;
	default:
		break;
	}
	mpRect.y = yy;
	mpDepth = -mpRect.y;


	//!for the left right value, that uses MRUA
	if (mpDirectionX != LEFT && mpDirectionX != RIGHT) {
		mpSpeedX *= mpFriction;
	}
	else if (mpDirectionX == LEFT) {

		mpSpeedX += global_delta_time;
		mpSignDirection = -1;
	}
	else if (mpDirectionX == RIGHT) {
		mpSpeedX += global_delta_time;
		mpSignDirection = 1;
	}

	mpRect.x = xx + (mpSpeedX*global_delta_time / 1000) * mpSignDirection;

	checkCollisionWithMap();
	return;
}

bool Entity::checkCollisionWithMap() {
	int xx1 = mpRect.x;
	int xx2 = mpRect.x + mpRect.w;
	int yy1 = mpRect.y;
	int yy2 = mpRect.y + mpRect.h;

	switch(mpDirectionY){
	default: return true; break;
	case UP: case DOWN:
		xx1 += 1;
		xx2 -= 1;
		break;
	}
	switch(mpDirectionX){
	default: break;
	case LEFT: case RIGHT:
		yy1 += 1;
		yy2 -= 1;
		break;
	}

	bool chk1 = false;
	bool chk2 = false;
	int tilePos = 0;

	switch (mpDirectionY) {
	case UP:
		chk1 = sMapManager->getCollision(xx1 / TILE_SIZE, yy1 / TILE_SIZE);
		chk2 = sMapManager->getCollision(xx2 / TILE_SIZE, yy1 / TILE_SIZE);
		if (chk1 || chk2) {
			if (checkCollisionLine(xx1 + TILE_SIZE / 4, yy1, xx2 - TILE_SIZE / 4, yy1, 5) || (chk1 && chk2)) {
				tilePos = yy1 / TILE_SIZE * TILE_SIZE + TILE_SIZE;
				mpRect.y = tilePos;
			}
			else {
			}
		}
		else {
			if (checkCollisionLine(xx1 + TILE_SIZE / 4, yy1, xx2 - TILE_SIZE / 4, yy1, 5)) {
				tilePos = yy1 / TILE_SIZE * TILE_SIZE + TILE_SIZE;
				mpRect.y = tilePos;
			}
		}
		break;
	case DOWN:
		chk1 = sMapManager->getCollision(xx1 / TILE_SIZE, yy2 / TILE_SIZE);
		chk2 = sMapManager->getCollision(xx2 / TILE_SIZE, yy2 / TILE_SIZE);
		if (chk1 || chk2) {
			if (checkCollisionLine(xx1 + TILE_SIZE / 4, yy2, xx2 - TILE_SIZE / 4, yy2, 5) || (chk1 && chk2)) {
				tilePos = yy2 / TILE_SIZE * TILE_SIZE;
				mpRect.y = tilePos - mpRect.h;
			}
			else {

			}
		}
		else {
			if (checkCollisionLine(xx1 + TILE_SIZE / 4, yy2, xx2 - TILE_SIZE / 4, yy2, 5)) {
				tilePos = yy2 / TILE_SIZE * TILE_SIZE;
				mpRect.y = tilePos - mpRect.h;
			}
		}
		break;
	}

	switch(mpDirectionX){
	case LEFT:
		chk1 = sMapManager->getCollision(xx1 / TILE_SIZE, yy1 / TILE_SIZE);
		chk2 = sMapManager->getCollision(xx1 / TILE_SIZE, yy2 / TILE_SIZE);
		if (chk1 || chk2) {
			if (checkCollisionLine(xx1, yy1 + TILE_SIZE / 4, xx1, yy2 - TILE_SIZE / 4, 5) || (chk1 && chk2)) {
				tilePos = xx1 / TILE_SIZE * TILE_SIZE + TILE_SIZE;
				mpRect.x = tilePos;
			}
			else {
				if (chk1) {
					tilePos = yy1 / TILE_SIZE * TILE_SIZE + TILE_SIZE;
					mpRect.y = tilePos;
				}
				else {
					tilePos = yy2 / TILE_SIZE * TILE_SIZE;
					mpRect.y = tilePos - mpRect.h;
				}
			}
		}
		else {
			if (checkCollisionLine(xx1, yy1 + TILE_SIZE / 4, xx1, yy2 - TILE_SIZE / 4, 5)) {
				tilePos = xx1 / TILE_SIZE * TILE_SIZE + TILE_SIZE;
				mpRect.x = tilePos;
			}
		}
		break;
	case RIGHT:
		chk1 = sMapManager->getCollision(xx2 / TILE_SIZE, yy1 / TILE_SIZE);
		chk2 = sMapManager->getCollision(xx2 / TILE_SIZE, yy2 / TILE_SIZE);
		if (chk1 || chk2) {
			if (checkCollisionLine(xx2, yy1 + TILE_SIZE / 4, xx2, yy2 - TILE_SIZE / 4, 5) || (chk1 && chk2)) {
				tilePos = xx2 / TILE_SIZE * TILE_SIZE;
				mpRect.x = tilePos - mpRect.w;
			}
			else {
				if (chk1) {
					tilePos = yy1 / TILE_SIZE * TILE_SIZE + TILE_SIZE;
					mpRect.y = tilePos;
				}
				else {
					tilePos = yy2 / TILE_SIZE * TILE_SIZE;
					mpRect.y = tilePos - mpRect.h;
				}
			}
		}
		else {
			if (checkCollisionLine(xx2, yy1 + TILE_SIZE / 4, xx2, yy2 - TILE_SIZE / 4, 5)) {
				tilePos = xx2 / TILE_SIZE * TILE_SIZE;
				mpRect.x = tilePos - mpRect.w;
			}
		}
		break;
	}

	return sMapManager->getCollision(mpRect.x / TILE_SIZE, mpRect.y / TILE_SIZE);
}

bool Entity::checkCollisionLine(int x1, int y1, int x2, int y2, int dp) {
	if (x1 == x2) { //Vertical
		int yMin = min(y1, y2);
		int yMax = max(y1, y2);
		for (int y = yMin; y < yMax; y += dp) {
			if (sMapManager->getCollision(x1 / TILE_SIZE, y / TILE_SIZE)) {
				return true;
			}
		}
	}
	else if (y1 == y2) { //Horizontal
		int xMin = min(x1, x2);
		int xMax = max(x1, x2);
		for (int x = xMin; x < xMax; x += dp) {
			if (sMapManager->getCollision(x / TILE_SIZE, y1 / TILE_SIZE)) {
				return true;
			}
		}
	}

	return false;
}


void Entity::setX(int x) {
	mpRect.x = x;
	return;
}

void Entity::setY(int y) {
	mpRect.y = y;
	mpDepth = -mpRect.y;
	return;
}

void Entity::setXY(int x, int y) {
	setX(x);
	setY(y);
	return;
}

void Entity::setW(int w) {
	mpRect.w = w;
	return;
}

void Entity::setH(int h) {
	mpRect.h = h;
	return;
}

void Entity::setRectangle(C_Rectangle rect) {
	mpRect = rect;
	return;
}

void Entity::setRectangle(int x, int y, int w, int h) {
	C_Rectangle a_rect = { x, y, w, h };
	setRectangle(a_rect);
	return;
}

bool Entity::isInsideRectangle(C_Rectangle a_rect) {
	if (C_RectangleTouch(mpRect, a_rect)) {
		return true;
	}
	return false;
}

void Entity::setAlive(bool alive) {
	mpAlive = alive;
	return;
}

bool Entity::isOfClass(std::string classType) {
	if (classType == "Entity") {
		return true;
	}
	return false;
}

